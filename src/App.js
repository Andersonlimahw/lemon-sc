import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import Atom from '../src/components/containers/atom';
import  Molecule from './components/containers/molecule';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
        </header>
        <Atom />
        <Molecule />
      </div>
    );
  }
}

export default App;
