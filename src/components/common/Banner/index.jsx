import React from 'react';

import BannerContainer from './BannerContainer';
import BannerTitle from './BannerTitle';
import BannerDescription from './BannerDescription';
import BannerThumb from './BannerThumb';
import Button from '../Button';

const Banner = ({
    title, 
    description, 
    big, 
    secondary,
    thumbUrl, 
    actionLabel, 
    actionClick}) => (
    <BannerContainer big={big} secondary={secondary}>
        { thumbUrl ? <BannerThumb src={thumbUrl} alt='thumb of Banner'/> : null }
        <BannerTitle big={big} secondary={secondary}> {title} </BannerTitle>
        <BannerDescription big={big} secondary={secondary}> 
            {description} 
            <br/>
        { actionLabel ?
            <Button 
                secondary={secondary} 
                square
                onClick={actionClick} >{
                actionLabel}
            </Button> : 
        null }
        </BannerDescription>
        
    </BannerContainer>
);

export default Banner;
