import styled from 'styled-components';

export const BannerDescription = styled.p`
    color: ${props => props.secondary ?  'green' : 'rgba(0,0,0, 0.7)'};
    font-size: ${props => props.big ?  '1.5rem' : '0.9rem'};
    font-weight: medium;
    padding: 0.5rem 0;
    margin: 0;
    text-align: left;

    button {
        margin-left: 0;
    }
`;

export default BannerDescription;