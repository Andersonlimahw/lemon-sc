import styled from 'styled-components';

export const BannerContainer = styled.div`
   display: inline-block;
   width: 100%;
   padding: 0;
   margin: 2rem 0;
   border-bottom: 2px solid #dadada;
`;

export default BannerContainer;