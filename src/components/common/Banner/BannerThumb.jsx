import styled from 'styled-components';

export const BannerThumb = styled.img`
   width: 70%;
   height: auto;
   float: right;
   margin: 0;
`;

export default BannerThumb;