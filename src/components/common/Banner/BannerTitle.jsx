import styled from 'styled-components';

export const BannerTitle = styled.h3`
    color: ${props => props.secondary ?  'green' : 'rgba(0,0,0, 0.9)'};
    font-size: ${props => props.big ?  '2.5rem' : '1.5rem'};;
    padding: 0.5rem 0;
    margin: 5rem 0 0 0;
    font-weight: bold;
    text-align: left;
`;

export default BannerTitle;