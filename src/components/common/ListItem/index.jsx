import React from 'react';
import TitleItem from './TitleItem';
import DescriptionItem from './DescriptionItem';

const ListItem = ({title, description, big, secondary}) => (
    <div>
        <TitleItem big={big} secondary={secondary}> {title} </TitleItem>
        <DescriptionItem big={big} secondary={secondary}> {description} </DescriptionItem>
    </div>
);

export default ListItem;
