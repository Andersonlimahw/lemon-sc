import styled from 'styled-components';

export const TitleItem = styled.p`
    color: ${props => props.secondary ?  'green' : 'rgba(0,0,0, 0.9)'};
    font-size: ${props => props.big ?  '2rem' : '1.2rem'};;
    padding: 0.5rem 0;
    margin: 0;
    font-weight: bold;
    text-align: left;
`;

export default TitleItem;