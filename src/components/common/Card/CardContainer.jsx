import styled from 'styled-components';

export const CardContainer = styled.div`
   display: block;
   position: static;
   float: left;
   width: ${props => props.big ? '90%' : '300px'};
   border: 1px solid #dadada;
   padding: 1rem;
   margin: ${props => props.group && !props.big ? '1rem 1rem' : '1rem auto'};
   :hover {
       box-shadow: -1px 6px 7px #666;
       border-top: 2px solid ${props => props.secondary ? 'green':  '#333'};
   }
`;

export default CardContainer;