import styled from 'styled-components';

export const CardFooter = styled.div`
    padding: 1rem;
    margin: 0 auto;
    text-align: left;
`;

export default CardFooter;