import React from 'react';

import CardContainer from './CardContainer';
import CardTitle from './CardTitle';
import CardDescription from './CardDescription';
import CardThumb from './CardThumb';
import CardFooter from './CardFooter';
import Button from '../Button';

const Card = ({
    title, 
    description, 
    big, 
    secondary,
    group, 
    thumbUrl, 
    actionLabel, 
    actionClick}) => (
    <CardContainer group={group} big={big} secondary={secondary}>
        { thumbUrl ? <CardThumb src={thumbUrl} alt='thumb of card'/> : null }
        <CardTitle big={big} secondary={secondary}> {title} </CardTitle>
        <CardDescription big={big} secondary={secondary}> {description} </CardDescription>
        <CardFooter big={big}>
            <Button onClick={actionClick} secondary={secondary}>{actionLabel}</Button>
        </CardFooter>
    </CardContainer>
);

export default Card;
