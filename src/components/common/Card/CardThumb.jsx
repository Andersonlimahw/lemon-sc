import styled from 'styled-components';

export const CardThumb = styled.img`
   width: 100%;
   height: auto;
   border-bottom: ${props => props.border ? '2px solid #333' : 'none' };
   margin: -1rem 0 1rem 0rem;
`;

export default CardThumb;