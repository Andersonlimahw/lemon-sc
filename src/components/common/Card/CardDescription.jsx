import styled from 'styled-components';

export const CardDescription = styled.p`
    color: ${props => props.secondary ?  'green' : 'rgba(0,0,0, 0.7)'};
    font-size: ${props => props.big ?  '1.5rem' : '0.9rem'};
    font-weight: medium;
    padding: 0.5rem 0;
    margin: 0;
    text-align: left;
`;

export default CardDescription;