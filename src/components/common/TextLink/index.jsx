import styled from 'styled-components';
/* user unicode to icons, ref: https://azuliadesigns.com/html-character-codes-ascii-entity-unicode-symbols/ */

export const TextLink = styled.a`
    color: ${props => props.secondary ? 'green' : '#333'};
    font-weight: bold;
    font-size: 1rem;
    border-bottom: 2px solid ${props => props.secondary ? 'green' : '#333'};
    padding-bottom: 0.25rem;
    transition: all 0.2s ease-in;
    margin: 1rem;

    :hover {
        border-bottom: 4px solid ${props => props.secondary ? 'green' : '#333'};
        cursor: pointer;
    }

    ::before {
        content: '${props => props.icon ? props.icon : ''}';
        margin-right: 0.25rem;
    }
`;

export const LemonLink = styled(TextLink)`
    color: green;
    border-color: green;

    :hover {
        border-color: green;
    }
`;

export default TextLink;