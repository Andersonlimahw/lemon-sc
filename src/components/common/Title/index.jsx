import styled from 'styled-components';

export const Title = styled.h1`
    color: rgba(0,0,0, 0.8);
    text-shadow: 1px 2px #f2f2f2;
    font-size: 2.5em;
`;

export default Title;
