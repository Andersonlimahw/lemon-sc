import styled from 'styled-components';

const Wrapper = styled.div`
    background-color: #fff;
    min-height: 100vh;
    height: auto;
    color: #333;
    padding: 16px 64px;
    /* display: flex; */
    flex-direction: column;
    align-items: left;
    justify-content: left;
`;

export default Wrapper;