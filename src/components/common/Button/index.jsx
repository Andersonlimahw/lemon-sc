import styled from 'styled-components';

export const Button = styled.button`
    color: ${props => props.secondary ? 'green' : '#333'};
    border: 2px solid ${props => props.secondary ? 'lemonchiffon' : '#333'};
    background: ${props => props.secondary ? 'lemonchiffon' : '#fff'};
    padding: 1.5em 3em;
    font-size: 1rem;
    margin: 1rem;
    border-radius: ${props => props.square ? '0px'  : '2.5rem'};
    outline: none;
    
    :hover {
        opacity: 0.9;
        cursor: pointer;
        box-shadow: 1px  2px 2px #333;
    }

    :disabled {
        color: #333;
        background: #dadada;
        border: #dadada;
        cursor: default;
        box-shadow: none;
    }
`;

export const LineButton = styled(Button)`
    background: lemonchiffon;
    border-color: lemonchiffon;
    color: #333;
`
export default Button;