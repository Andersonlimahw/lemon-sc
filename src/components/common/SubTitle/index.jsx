import styled from 'styled-components';

export const SubTitle = styled.h3`
    color: rgba(0,0,0, 0.7);
    text-shadow: 1px 2px #f9f9f9;
    font-size: 1.5em;
    padding: 1rem 0;
`;

export const  TomatoSubTitle = styled(SubTitle)`
    color: tomato;
`;

export default SubTitle;
