import React from 'react';

import Wrapper from '../../common/Wrapper';
import SubTitle from '../../common/Title';
import ListItem from '../../common/ListItem';
import Card from '../../common/Card';
import Banner from '../../common/Banner';

export default class Molecule extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Wrapper>
                <SubTitle>Molecule</SubTitle>


                <Banner 
                    title='Banner default title'
                    description='This banner have a amazing division'
                    actionLabel='buy'
                    thumbUrl='http://picandocodigo.net/wp-content/uploads/2018/01/marvel-legacy.jpg'
                />

                <Banner 
                    title='Banner secondary title'
                    description='This banner is of the best movie using secondary variation'
                    secondary
                    actionLabel='buy'
                    actionClick={()=> alert('Clicked on avenger banner')}
                    thumbUrl='http://picandocodigo.net/wp-content/uploads/2018/01/marvel-legacy.jpg'
                />

                <ListItem title='List item big' description='description of list item' big />
                <ListItem title='List item title' description='description of list item' />
                <ListItem title='List big secondary' description='description of list item' big secondary/>
                <ListItem title='List item title' description='description of list item' secondary/>

                <Card 
                    title='Card title'
                    description='This card have a amazing division'
                    secondary
                    actionLabel='next'
                    big
                />

                <div>
                <Card 
                    title='Card title'
                    description='This card have a amazing division'
                    secondary
                    actionLabel='next'
                    thumbUrl='https://vignette.wikia.nocookie.net/starwars/images/9/94/Vader_chokes_Aphra.png/revision/latest?cb=20151119054721'
                    group
                />

                <Card 
                    title='Card title'
                    description='This card have a amazing division'
                    actionLabel='next'
                    thumbUrl='https://vignette.wikia.nocookie.net/starwars/images/9/94/Vader_chokes_Aphra.png/revision/latest?cb=20151119054721'
                    group
                />

                <Card 
                    title='Card title'
                    description='This card have a amazing division'
                    secondary
                    actionLabel='next'
                    thumbUrl='https://vignette.wikia.nocookie.net/starwars/images/9/94/Vader_chokes_Aphra.png/revision/latest?cb=20151119054721'
                    group
                />
                </div>
                <Card 
                    title='Card title'
                    description='This card have a amazing division'
                    secondary
                    actionLabel='next'
                    thumbUrl='https://vignette.wikia.nocookie.net/starwars/images/9/94/Vader_chokes_Aphra.png/revision/latest?cb=20151119054721'
                    big
                />
                

               
                
            </Wrapper>
        );
    }
}
