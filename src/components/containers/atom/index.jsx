import React from 'react';

import Button from '../../common/Button';
import Title from '../../common/Title';
import SubTitle from '../../common/SubTitle';
import TextLink from '../../common/TextLink';
import Wrapper from '../../common/Wrapper';

export class Atom extends React.Component {

    constructor(props) {
        super(props);
    }    
    
    componentDidMount() {
        console.log(`Atom container did mount`);
    }

    render() {
        return (
            <Wrapper>
                <Title>Atom: Styled components</Title>
                <SubTitle>Buttons</SubTitle>
                <Button> Button </Button>
                <Button secondary> Secondary </Button>
                <Button disabled>Disabled</Button>

                <SubTitle>Link</SubTitle>
                <TextLink>Link example</TextLink>
                <TextLink secondary>Link secondary</TextLink>
                <TextLink icon='&#10084;' >Link icon</TextLink>
                <TextLink icon='&#9734;'>Star icon</TextLink>

            </Wrapper>
        );
    }
}

export default Atom;